/*
* GccApplication4.cpp
*
* Created: 03.05.2017 19:56:14
* Author : Юра
*/

#define F_CPU 16000000L
#define XTAL F_CPU
#define baudrate 9600L
#define bauddivider (XTAL/(16*baudrate)-1)
#define HI(x) ((x)>>8)
#define LO(x) ((x)& 0xFF)

#define CHR_ESC				0x1B
#define CHR_BS				0x08
#define CHR_CR				0x0D
#define CHR_LF				0x0A


#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <avr/interrupt.h>

#include <stdbool.h>
#include <avr/cpufunc.h>
#include <string.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>
#include "fifo.h"



//Global variables here

#define RX_BUFFER_SIZE 16
char rx_buffer[RX_BUFFER_SIZE];
unsigned char rx_wr_index,rx_rd_index,rx_counter;
char rx_buffer_overflow;
char read_enable = 0;
char process_data = 0;
volatile unsigned char trigger=1;
volatile unsigned char direction_pwm_led=1;
volatile unsigned char direction_bam=1;
volatile unsigned char pwm_led_counter=0;
volatile unsigned char need_succ;
unsigned char EEMEM need_succ_mem=1;
volatile unsigned char divider_pwm_led;
unsigned char EEMEM divider_pwm_led_mem=1;
volatile uint16_t vizualized_pwm0,vizualized_pwm1;
volatile uint16_t low_border,high_border;
uint16_t EEMEM low_border_mem=0,high_border_mem=1023;
volatile uint16_t sleep_off,sleep_off_cur=0;
uint16_t EEMEM sleep_off_mem=0;
uint16_t EEMEM function_mem=0;
unsigned char EEMEM shifttest_mem=3;

volatile unsigned char shifttest=3;
volatile unsigned char maxtest=9;

volatile uint16_t flagFunction=30;
volatile unsigned char counter=0;
volatile float a_=0;
volatile float a_max=0;
volatile float a_min=0;
volatile int succ_cnt=0;

#define PWM_LED_NUM 1

#define N_PWM 12
volatile uint16_t pwm[N_PWM], direct[N_PWM]; //pwm - непосредственно значения BAM для светодиодов, direct - направление изменения яркостей для эффектов
unsigned char mask_b, mask_c, mask_d;
unsigned char stage=0;
uint16_t EEMEM pwm_mem[12]={0x3FF,0x3FF,0x3FF,0x3FF,0x3FF,0x3FF,0x3FF,0x3FF,0x3FF,0x3FF,0x3FF,0x3FF};

FIFO(256 ) uart_tx_fifo;
//FIFO( 32 ) uart_rx_fifo;

void TimerLEDInit(void)
{

	// set up timer with prescaler = 1024
	TCCR0 |= (1 << CS02)| (0 << CS01)| (1 << CS00);
	
	// initialize counter
	TCNT0 = 0;
	
	// enable overflow interrupt
	TIMSK |= (1 << TOIE0);
}

void TimerInit(void)
{

	// Timer/Counter 1 initialization
	// Clock source: System Clock
	// Clock value: 8000,000 kHz
	// Mode: CTC top=OCR1A
	// OC1A output: Discon.
	// OC1B output: Discon.
	// Noise Canceler: Off
	// Input Capture on Falling Edge
	// Timer 1 Overflow Interrupt: On
	// Input Capture Interrupt: Off
	// Compare A Match Interrupt: Off
	// Compare B Match Interrupt: Off
	TCCR1A = (0<<COM1A1)|(0<<COM1A0)|(0<<COM1B1)|(0<<COM1B0)|(0<<FOC1A)|(0<<FOC1B)|(0<<WGM11)|(0<<WGM10);
	//TCCR1B = (0<<ICNC1) |(0<<ICES1) |(0)        |(0<<WGM13) |(0<<WGM12)|(0<<CS12) |(1<<CS11) |(1<<CS10);
	TCCR1B=0;
	TCNT1H = 0x00;
	TCNT1L = 0x00;
	ICR1H  = 0x00;
	ICR1L  = 0x00;
	OCR1AH = 0x00;
	//OCR1AL = 0x10;
	OCR1AL = 0x00;
	OCR1BH = 0x00;
	OCR1BL = 0x00;
	TIMSK  = (1<<TOIE1);
}

void USARTInit(void)
{
	UBRRH = HI(bauddivider);
	UBRRL = LO(bauddivider);
	UCSRB = 1<<RXEN|1<<TXEN|1<<RXCIE;//|1<<TXCIE;
	//ucsrb recieveENABPWM transmitENABLE
	//recieveINTERenable transmitINTERdisable
	UCSRC = 1<<URSEL|1<<UCSZ0|1<<UCSZ1; //8bit
}

void uart_send( char data)
{
	FIFO_PUSH(uart_tx_fifo, data);
}
void uart_send_( char data)
{
	while(!( UCSRA & (1 << UDRE)));   // Ожидаем когда очистится буфер передачи
	UDR = data; // Помещаем данные в буфер, начинаем передачу
}
void uart_send_str( char *s)
{
	while (*s != 0) uart_send(*s++);
}

uint16_t uint16ifmem(uint16_t a,uint16_t max,uint16_t thenn ){if (a==max) {return thenn;}else{return a;}}
char chrifmem(char a,char max,char thenn ){if (a==max) {return thenn;}else{return a;}}
unsigned char uchrifmem(unsigned char a,unsigned char max,unsigned char thenn ){if (a==max) {return thenn;}else{return a;}}
	
void uart_send_str_(void)
{
	while (FIFO_COUNT(uart_tx_fifo) != 0)
	{
		uart_send_(FIFO_FRONT(uart_tx_fifo));
		FIFO_POP(uart_tx_fifo);
	}
}
void uart_clear_screen(void)
{
	uart_send(CHR_ESC);
	uart_send_str("[2J");
	uart_send(CHR_ESC);
	uart_send_str("[H");
}
void uart_clear_line(void)
{
	uart_send(CHR_ESC);
	uart_send_str("[2K");
	uart_send(CHR_ESC);
	uart_send_str("[99D");

}
void uart_new_line(void)
{
	uart_send(CHR_CR);
	uart_send(CHR_LF);
}

uint16_t strtoint(const char * str,unsigned char start,unsigned char count)
{
	char valuePWM[count];
	memcpy(valuePWM,&str[start],count);
	return atoi(valuePWM);
}

ISR(TIMER0_OVF_vect)
{
	
	counter++;
	if (counter==50){
		counter=0;
		uart_send_str("W");	
		PORTC=(1<<0);
	
		_delay_us(20);
		PORTC=0;
		
		}
		
}


ISR(USART_RXC_vect)
{
	char data=UDR;
	
	if ((data == 'x') || (data == 'X'))
	{
		memset(&rx_buffer[0], 0, sizeof(rx_buffer));
		rx_wr_index = 0;
		read_enable = 0;
		uart_clear_line();
	}
	else{
		if (read_enable == 0)
		{
			memset(&rx_buffer[0], 0, sizeof(rx_buffer));
			rx_wr_index = 0;
			read_enable = 1;
		}
		if(((data == 13)||((data == 10)))&&(read_enable == 1))
		{
			uart_send('.');
			uart_new_line();
			uart_send('>');
			read_enable = 0;
			process_data = 1;
		}
		
		if (read_enable == 1)
		{
			uart_send(data);
			rx_buffer[rx_wr_index++]=data;
			if (rx_wr_index == RX_BUFFER_SIZE)
			rx_wr_index=0;
			if (++rx_counter == RX_BUFFER_SIZE)
			{
				rx_counter=0;
				rx_buffer_overflow=1;
			}
		}
	}
}

ISR(INT0_vect)
{
	TCCR1B=0;
	char str1[15];
	float a=TCNT1/250.000000*1000.000000/58.000000;
	a_=a;
	sprintf(str1, "%u-%.1f-%u",low_border,a,high_border);
	uart_send_str(str1);
	uart_new_line();
	if ((a<high_border)&&(a>low_border))
	succ_cnt++;
	else {succ_cnt=0; PORTD |= (1 << PD7);}
		
		
		if (succ_cnt>=need_succ)
		PORTD &= ~(1 << PD7);
	
}
ISR(INT1_vect)
{
	
	TCNT1=0;
	TCCR1B = (0<<ICNC1) |(0<<ICES1) |(0)        |(0<<WGM13) |(0<<WGM12)|(0<<CS12) |(1<<CS11) |(1<<CS10);
}

	
	ISR(TIMER1_OVF_vect)
	{
		uart_send_str("a");
		//TCCR1B=0;
	}


int main(void)
{
	DDRB = 0xFF;
	
	DDRC = 0xFF;
	DDRD = (1<<PORTD0)|(1<<PORTD1)|(0<<PORTD2)|(0<<PORTD3)|(1<<PORTD4)|(1<<PORTD5)|(1<<PORTD6)|(1<<PORTD7);
	PORTD=(0<<PORTD0)|(0<<PORTD1)|(0<<PORTD2)|(0<<PORTD3)|(0<<PORTD4)|(0<<PORTD5)|(0<<PORTD6)|(0<<PORTD7);
	PORTB=0;
	_delay_ms(250);
	cli();
	USARTInit();
	need_succ = chrifmem(eeprom_read_byte(&need_succ_mem),0xff,5);
	divider_pwm_led = chrifmem(eeprom_read_byte(&divider_pwm_led_mem),0xff,0);
	high_border = uint16ifmem(eeprom_read_word(&high_border_mem),0xffff,15);
	low_border = uint16ifmem(eeprom_read_word(&low_border_mem),0xffff,0);
	sleep_off = uint16ifmem(eeprom_read_word(&sleep_off_mem),0xffff,0);
	flagFunction = uint16ifmem(eeprom_read_word(&function_mem),0xffff,31);
	shifttest = chrifmem(eeprom_read_byte(&shifttest_mem),0xff,3);
	TimerInit();
	TimerLEDInit();
	MCUCR =(1<<ISC10)|(1<<ISC11)|(0<<ISC00)|(1<<ISC01);
	GICR=(1<<INT0)|(1<<INT1);
	//TCCR1B = (0<<ICNC1) |(0<<ICES1) |(0)        |(0<<WGM13) |(1<<WGM12)|(0<<CS12) |(1<<CS11) |(0<<CS10);
	

	
	
	for(unsigned char i = 0; i < N_PWM; i++)
	{
		pwm[i] = uint16ifmem(eeprom_read_word(&pwm_mem[i]),0xffff,1023);
		direct[i] = 0;
	}

	

	sei();

	while(1)
	{	
		if (FIFO_COUNT(uart_tx_fifo)!=0) uart_send_str_();
		//if (!(PIND & (1<<PD4))) 
		//{
		//	a_min=a_*0.9-1;
		//	a_max=a_*1.1+1;
		//	
		//}
		
		if (process_data==1)
		{
			process_data=0;
			if (!strncmp(rx_buffer,"pw",2))	//pwa 100 //
			{								//01234567
				pwm[rx_buffer[2]-'a']=strtoint(rx_buffer,4,3)/100.0*1023;
				eeprom_write_word(&pwm_mem[rx_buffer[2]-'a'], pwm[rx_buffer[2]-'a']);
			}
			else if (!strncmp(rx_buffer,"su",2))	//sp 190 //speed of changing led pwm value
			{										//012345
				need_succ=strtoint(rx_buffer,3,3);
				eeprom_write_byte(&need_succ_mem, need_succ);
			}
			else if (!strncmp(rx_buffer,"di",2))	//di 3 //divider
			{										//012345
				divider_pwm_led=strtoint(rx_buffer,3,3);
				eeprom_write_byte(&divider_pwm_led_mem, divider_pwm_led);
			}
			else if (!strncmp(rx_buffer,"lo",2))	//lo 200 //low border led change
			{										//012345
				low_border=strtoint(rx_buffer,3,4);
				eeprom_write_word(&low_border_mem, low_border);
			}
			else if (!strncmp(rx_buffer,"hi",2))	//hi 1023 //high border led change //hi 600
			{										//0123456
				high_border=strtoint(rx_buffer,3,4);
				eeprom_write_word(&high_border_mem, high_border);
			}
			else if (!strncmp(rx_buffer,"sl",2))	//sl 0 //time to sleep change - led //sl 300
			{										//01234567
				sleep_off=strtoint(rx_buffer,3,5);
				eeprom_write_word(&sleep_off_mem, sleep_off);
			}
			else if (!strncmp(rx_buffer,"fu",2))	//fu 1020 //function change
			{										//0123456
				flagFunction=strtoint(rx_buffer,3,4);
				eeprom_write_word(&function_mem, flagFunction);
			}
			else if (!strncmp(rx_buffer,"dd",2))	//dd 3 //shift BAM bit
			{										//0123
				shifttest=rx_buffer[3]-'0';
				eeprom_write_byte(&shifttest_mem, shifttest);
			}
			//else if (!strncmp(rx_buffer,"fa",2))	//fa0 1020 //fan speed change
			//{										//01234567
			//	pwm[strtoint(rx_buffer,2,1)]=strtoint(rx_buffer,4,4);
			//}
			else if (!strncmp(rx_buffer,"cl",2)) uart_clear_screen();
			else if (!strncmp(rx_buffer,"in",2))
			{
				char str1[100];
				sprintf(str1, "su:%d div:%d low:%u hi:%u slp:%u sft:%u bit:%u",need_succ,divider_pwm_led,low_border,high_border,sleep_off,shifttest,maxtest);
				uart_send_str(str1);
				uart_new_line();
			}
			else if (!strncmp(rx_buffer,"ww",2))	//wwa
			{										//012
				char str1[6];
				sprintf(str1, "%u",pwm[rx_buffer[2]-'a']);
				uart_send_str(str1);
				uart_new_line();
			}
			// 			else if (!strncmp(rx_buffer,"ee",2))
			// 			{
			// 				char str1[10];
			// 				sprintf(str1, "%d",strtoint("uu 1234",3,3));
			// 				uart_send_str(str1);
			// 				uart_new_line();
			// 			}


			else
			{
				uart_send_str("Unkn cmd");
				uart_new_line();
				
			}
		}
	}
}